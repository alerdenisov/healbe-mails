## Installation

Clone repository on local machine with git client: `git clone https://alerdenisov@bitbucket.org/alerdenisov/healbe-mails.git` and install all dependencies with NPM: `npm install`

## Build and Running commands

Run `npm start` to kick off the build process. A new browser tab will open with a server pointing to your project files.

Run `npm run build` to inline your CSS into your HTML along with the rest of the build process.

Run `npm run zip` to build as above, then zip HTML and images for easy deployment to email marketing services. 

## Urls
* http://localhost:3000/announce-0-android.html 
* http://localhost:3000/announce-0-ios.html 
* http://localhost:3000/beta-tester.html
* http://localhost:3000/confirmation.html
* http://localhost:3000/no-weekly-data.html
* http://localhost:3000/weekly-data.html
